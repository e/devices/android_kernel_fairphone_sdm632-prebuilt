#
# Copyright (C) 2022 E FOUNDATION
#
# SPDX-License-Identifier: Apache-2.0
#

DEVICE_KERNEL_PATH := kernel/fairphone/sdm632-prebuilt

# Kernel
TARGET_PREBUILT_KERNEL := $(DEVICE_KERNEL_PATH)/Image.gz-dtb
TARGET_FORCE_PREBUILT_KERNEL := true

# DTBO
BOARD_PREBUILT_DTBOIMAGE := $(DEVICE_KERNEL_PATH)/dtbo.img
